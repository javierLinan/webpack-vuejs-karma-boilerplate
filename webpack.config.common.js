module.exports = {
  module: {
    preLoaders: [
      {
        test: /(\.js|\.vue)$/,
        exclude: /node_modules/,
        loader: "eslint"
      }
    ],
    loaders: [
      {
        test: /\.js$/, // include .js files
        exclude: /node_modules/, // exclude any and all files in the node_modules folder
        loader: "babel"
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: "vue"
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url',
        query: {
          // limit for base64 inlining in bytes
          limit: 10000,
          // custom naming format if file is larger than
          // the threshold
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  vue: {
    autoprefixer: {
      browsers: ["last 2 versions"]
    }
  }
};
